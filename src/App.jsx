import React, { Component } from 'react';
import logo from './logo.svg';
import './App.css';

/**
 * Config firebase firestore
 */
import firebase from 'firebase'
import 'firebase/firestore'

const config = {
  apiKey: "AIzaSyD9YiiV2s4nFrZP21s4igySzS68gElqUt4",
  authDomain: "react-my-test-93bb6.firebaseapp.com",
  databaseURL: "https://react-my-test-93bb6.firebaseio.com",
  projectId: "react-my-test-93bb6",
  storageBucket: "react-my-test-93bb6.appspot.com",
  messagingSenderId: "710842240130"
};
firebase.initializeApp(config);
const  db = firebase.firestore();
const settings = {timestampsInSnapshots: true};
db.settings(settings);

// --------------------------------

class App extends Component {
  constructor(props) {
    super(props)
    this.state = {
      text: '',
      posts: {}
    }
  }

  saveData() {
    db.collection('posts').add({
      text: this.state.text
    }).then((result) => {

      let parsedPosts = {...this.state.posts}
      parsedPosts = {
        ...parsedPosts,
        [result.id]: {id: result.id, text: this.state.text}
      }
      this.setState({
        posts: parsedPosts
      })

      console.log(result)
    })
    .catch((error) => {
      console.log(error)
    })
  }

  changeText(event) {
    this.setState({
      text: event.target.value
    })
  }

  deletePost(postId) {
    db.collection('posts').doc(postId).delete().then(() => {
      console.log(`Post by ${postId} id is deleted !`)
    })

    let parsedPosts = {}
    Object.keys(this.state.posts).forEach((key) => {
      if(key !== postId) {
        parsedPosts = {
          ...parsedPosts,
          [key]: this.state.posts[key]
        }
      }

    })
    this.setState({
      posts: parsedPosts
    })
  }


  postList() {
    const {posts} = this.state
   return Object.keys(posts).map((key) => {
      return (<li key={key}>
        <h4>{posts[key].id}</h4>
        <span>{posts[key].text}</span>
        <span><button onClick={() => this.deletePost(key)}>Delete</button></span>
        </li>)
    })
  }

  componentDidMount() {
    db.collection('posts').get()
    .then((result) => {
      let parsedPosts = {}

      result.forEach((post) => {
        parsedPosts = {
          ...parsedPosts,
          [post.id]: {id: post.id, ...post.data()}
        }
      })


      this.setState({
        posts: parsedPosts
      })
    })
  }

  render() {
    const {text, posts} = this.state
    return (
      <div className="App">
        <header className="App-header">
          <img src={logo} className="App-logo" alt="logo" />
          <h1 className="App-title">Welcome to React</h1>
        </header>
        <p className="App-intro">
          To get started, edit <code>src/App.js</code> and save to reload.
        </p>
        <div>
          <input type="text" name="text" onChange={this.changeText.bind(this)} value={text} id="text"/>
        <button onClick={this.saveData.bind(this)}> Send </button>  
        </div>
        <div>
          <h1>posts</h1>
          <ul>
            {
              this.postList()
            }
          </ul>
        </div>
      </div>
    );
  }
}

export default App;
